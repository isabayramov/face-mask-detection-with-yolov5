#!/bin/bash

#Pulling the newest dockerfile from our repository into a new folder from which we build the docker image. At the moment this is done by cloning the whole repository and deleting not needed files afterwards. Another option with slightly more manual work would be to simply copy the Dockerfile together with deploy.sh and put it in the correct folders. For the sake of illustration and minimization of potential errors we chose the way of cloning and deleting. As we are therefore already pulling the repository, we use this step to also copy the weights and the run.sh that we will use for inference into the same folder. This could also be done in a later step.
mkdir ~/Desktop/maskdetection
cd ~/Desktop/maskdetection

git clone  https://ruben:CzZsqzFkwRHaTLY-ns6e@git.scc.kit.edu/upqaq/AISS-CV-Mask-Group-F.git --depth 1

cd AISS-CV-Mask-Group-F/yolov5
cp Dockerfile ~/Desktop/maskdetection

cp -r runs/train/exp/weights/ ~/Desktop/maskdetection

cp -r deployment/run.sh ~/Desktop/maskdetection

cd ~/Desktop/maskdetection
#allow the execution of run.sh, this otherwise requires ticking a checkmark in the files properties
chmod +x run.sh

#we keep the folder to be able to use different sets of weights without rebuilding the container by simply mounting the correct weights in run.sh
#rm -rf AISS-CV-Mask-Group-F

#Preparing system for nvidia docker runtime by registering it with docker daemon, setting it as default runtime. This allows to use GPU during docker build command and at runtime
sudo tee /etc/docker/daemon.json << EOF
{
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    },
    "default-runtime": "nvidia"
}
EOF
sudo systemctl restart docker

#Building the docker image with the copied Dockerfile and naming it "yolov5"
sudo docker build -t yolov5 .

