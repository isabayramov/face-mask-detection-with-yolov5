# Quick-Start Guide 

This Guide aims to provide concrete steps to implement the developed model onto another Jetson Nano device with similar configuration. We also provide steps to use and deploy our model with a different dataset. For the practical implementation, we cloned a [repository](https://github.com/rkuo2000/yolov5) that is based on the model of Glen Rocher's [company ultralytics](https://ultralytics.com/). We used [Face Mask Detection](https://www.kaggle.com/andrewmvd/face-mask-detection) dataset from [kaggle platform](https://www.kaggle.com/) for pre training of our model.



## Repo Structure 

Our project name is **“AISS CV Mask Group F”**. This folder contains a notebook to train on Kaggle and a script to train mean/standard deviation for standardizing. We have another folder named **yolov5**, where all necessary parts of the project are stored. For all datasets, the structure below must hold. 

-       yolov5/Dataset/your_dataset_name/images/*.jpg --> containing images. 

-       yolov5/Dataset/your_dataset_name/annotations/annotations.csv --> containing image annotations (in COCO format) in one .csv file. 

-       yolov5/Dataset/your_dataset_name/labels/*.txt --> containing image annotations in multiple *.txt files, one per image. 

Note that all txt files must have the same name as the corresponding images.  

To train YOLO you need only the *.txt files. However, if you have one annotations.csv file (in COCO format) we provide a script that transforms the file into multiple *.txt files (as required for YOLO). The script is called to_yolo_format.py. (For more information see **Train Model with different dataset**) 

After splitting your data into train and validation sets, use the structure below. Alternatively, you can use our methods (For more information see **Train Model with different dataset**) to split the data and save it into the necessary folder structure. 
  
-       yolov5/Dataset/your_dataset_name/images/train/*.jpg 

-       yolov5/Dataset/your_dataset_name/images/test/*.jpg  

-       yolov5/Dataset/your_dataset_name/labels/train/*.txt

-       yolov5/Dataset/your_dataset_name/labels/test/*.txt  
    


## Necessary Steps to Deploy As-Is 

In this chapter we explain the necessary steps to deploy the model as-is on a NVIDIA Jetson Nano running Jetpack 4.5.1. This process only works if a CSI-camera is connected to Cam0. For additional help we provided a video that shows the deployment process (docker image already built) from end-to-end in "Deployment_Video.mp4".

1. **Step: Download deploy.sh**

        Download deploy.sh from this repository and save it on your Jetson Nano Device.  

1. **Step: Edit properties of deploy.sh**

        Right-click deploy.sh and go to “Properties”. 
        In the Permissions tab, tick the checkmark to “allow executing file as program”. 

1. **Step: Execute deploy.sh**

        Open the terminal in the same directory in which you saved deploy.sh and enter ./deploy.sh. 
        You might be asked for your password to allow sudo commands to be executed. 

1. **Step: Have patience!**

        This process will take several hours until completion upon the first installation. 
        The script will create a folder maskdetection in ⁓/Desktop and save necessary files for building the docker image used for deployment and ultimately running it for inference. 
        This step builds the docker image with the Dockerfile located in the repository in yolov5/.  

1. **Step: Execute run.sh**

        Once completed, navigate into the folder maskdetection that has been created on your Desktop. 
        Open the terminal in this directory and enter ./run.sh. 
        This will run the container and call detect.py of the YOLOv5 model. 
        Edit run.sh in the editor to change parameters for model inference such as confidence threshold or location of the weights to be used (check detect.py for a full list of supported arguments). 

In the rare event that run.sh does not successfully create an input stream and exits without launching the model correctly, simply relaunch run.sh (see chapter 5.5.5 for more information). This occurs, if at all, when the model is launched directly after building the container for the very first time. 

**Congratulations, you successfully deployed a YOLOv5 model onto your NVIDIA Jetson Nano!** 



## Train Model with different dataset 

Before training make sure all your images are in *.jpg format. Please use the folder structure described in **Chapter Repo Structure**. For training we recommend using jupyter notebook. 

1. **Modify facemask.yaml file** 

        Change path to train and validation dataset as below! 

        The train and validate folders will be created in step 4. 

        train: --> Dataset/ your_dataset_name /images/train 

        val: --> Dataset/ your_dataset_name /images/ validate 


1. **Calculate standardizing parameters (optional)** 

        mean_sigma_calculation.py --> creates normalize_parameters.yml. 
        You can adjust the output name. 
        This yaml file could be used as option for standardizing in training as well as inference. 
        Note that when deploying the model with standardized data with our deployment method (chapter Necessary Steps to Deploy As-Is),
        additional modification in detect.py is required to standardize the input data accordingly. 


1. **Extract Labels (optional if annotations are already in \*.txt files)** 

        yolo_format.py --> creates *.txt files for each image with bounding boxes in yolov5 format  

 
1. **Train & Validation Split** 

        train_own_data.py --> use train_test_split & move_data methods together (For the exact procedure see the main method of train_own_data.py.) 

        train_test_split --> this method defines indexes of training and validation images 

        move_data --> this method uses indexes above and moves train & validation images to related folders (For more information see Chapter Repo Structure)  


1. **Augmentation (optional)**
>**Because of a bug in Albumentations package, you need to make some changes if you want to use augmentation. For more information [==>](https://github.com/albumentations-team/albumentations/issues/459#issuecomment-734454278)**

        augmentation.py --> Augments each image once if there is no label “Incorrect_Mask”, otherwise it augments the image three times. 
        If you want to save the augmented images in a new folder, use the save_to parameter. 
        Otherwise all augmented images will be saved to same folder where images are read.  

 
1. **Train your Model**

        train.py --> please use --help for more information about training parameters or -->    

        train_own_data.py --> use train method for the training of your first model with our default values 


1. **Make a Prediction** 

        Detect.py --> please use --help for more information about parameters or -->    

        train_own_data.py --> use detection method to make a prediction for one image 
