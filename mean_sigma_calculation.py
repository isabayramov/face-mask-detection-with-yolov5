import os
import numpy as np
from PIL import Image

import yaml


def compute_normalizing_parameters(img_folder):
    
    mean = np.array([0,0,0], dtype=np.float64)
    sigma = np.array([0,0,0], dtype=np.float64)
    divide_by = 0 
    
    # compute mean
    for count, img_name in enumerate( os.listdir(img_folder) ):
        
        img = np.array( Image.open(img_folder+img_name) )
        mean += np.sum(img, axis=(0,1))
        
        size = np.shape( img )
        divide_by +=  size[0]*size[1]
        
    mean = mean/divide_by  
    
    # compute sigma
    for img_name in os.listdir(img_folder):
        img = np.array( Image.open(img_folder+img_name) ) 
        img = (img - mean)**2
        sigma += np.sum(img, axis=(0,1))
    
    sigma = np.sqrt( sigma/divide_by )
    
    # write mean & sigma in yaml file
    mean, sigma = mean/255, sigma/255  #here, we first normalize our mean and sigma between 0 and 1
    mean, sigma = mean.tolist(), sigma.tolist()
    data = dict( mean = mean, sigma = sigma )

    with open('yolov5/normalize_parameters.yml', 'w') as file:
        yaml.dump(data, file, default_flow_style=None)
    
    return data
    
    
if __name__ == "__main__":
    
    imgs_pth = "Dataset/own/images/"
    compute_normalizing_parameters(imgs_pth)