import pandas as pd
import os
import ast
import re
import albumentations as A
import cv2


"""Source Code: http://ralsina.me/weblog/posts/BB963.html"""
class DirContextM(object):
    def __init__(self, new_dir):
        self.new_dir = new_dir
        self.old_dir = None

    def __enter__(self):
        self.old_dir = os.getcwd()
        os.chdir(self.new_dir)

    def __exit__(self, *_):
        os.chdir(self.old_dir)


def to_yolo_fromat(path_to_dataset):
    
    with DirContextM(path_to_dataset):  
        df = pd.read_csv("annotations/annotations.csv")

        imgs = os.listdir("images")

        img_list = []

        for img in imgs:
            img_list.append(img)

        # pattern for finding the relevant class for the considered bbox
        pattern = '("no_mask":true)|("mask_incorrectly":true)|("mask_correctly":true)'

        for img in img_list:
            # create dataframe which will be written to txt file
            yolo_df = pd.DataFrame(columns=["class", "x_center", "y_center", "width", "height"])

            image = cv2.imread("images/" + str(img))
            # get width and height
            height, width = image.shape[0], image.shape[1]

            img_nm, ext = img.split(".")

            temp_df = (df.loc[df['filename'] == img])

            bbox = (temp_df['region_shape_attributes']).to_list()
            annots = (temp_df['region_attributes']).to_list()

            for i in range(len(bbox)):
                yolo_list = []

                # use pattern to find relevant class and assign class index (0 no mask, 1 incorrect, 2 correct)
                match = re.search(pattern, annots[i])[0]
                if match == '"no_mask":true':
                    cls = 0
                elif match == '"mask_incorrectly":true':
                    cls = 1
                elif match == '"mask_correctly":true':
                    cls = 2

                ##convert bboxes strings to dicts
                bbox_temp = ast.literal_eval(bbox[i])
                bbox_temp = (bbox_temp['x'], bbox_temp['y'], bbox_temp['width'], bbox_temp['height'])

                ## convert format to from coco to albumentations to yolo
                bbox_temp = A.augmentations.bbox_utils.convert_bbox_to_albumentations(bbox=bbox_temp, source_format='coco',
                                                                                      rows=height, cols=width)
                bbox_temp = A.augmentations.bbox_utils.convert_bbox_from_albumentations(bbox=bbox_temp,
                                                                                        target_format='yolo', rows=height,
                                                                                        cols=width)

                ## Append class and extend with bbox so resulting list looks like e.g. [1, 0.13, 0.32, 0.04, 0.07]
                yolo_list.append(cls)
                yolo_list.extend(bbox_temp)

                # append list to dataframe which will be written to txt for considered image
                yolo_df.loc[len(yolo_df)] = yolo_list
                
            if not os.path.exists('labels/'):
                os.makedirs('labels/')
                
            yolo_df['class'] = yolo_df['class'].astype(int)
            yolo_df.to_csv("labels/" + str(img_nm) + ".txt", sep=" ", index=False, header=False)


if __name__ == "__main__" :
    
    # set path to your dataset
    path_to_dataset = './Dataset/own/'
    
    # change annotations to yolo format
    to_yolo_fromat(path_to_dataset)
