import os
import subprocess
import shutil

from pathlib import Path
from shutil import move

from sklearn.model_selection import train_test_split as split_data

from utils.plots import plot_results 


# train test split
def train_test_split(img_dir):
    image_list = os.listdir(img_dir)
    train_list, validate_list = split_data(image_list, test_size=0.2, random_state=1)

    print('total :',len(image_list))
    print('train :',len(train_list))
    print('test  :',len(validate_list))
    
    return train_list, validate_list


def move_data(img_list, img_labels, imgs, type):
  
    root = Path(imgs + type)
    if not root.exists():
        print(f"{root} created")
        os.makedirs(root)

    root = Path(img_labels + type)
    if not root.exists():
        print(f"{root} created")
        os.makedirs(root)

    for img in img_list:
        img_name = img.replace('.jpg', '')
        img_name = img_name.replace('.JPG', '')
        img_source = imgs + img_name + '.jpg'
        label_source = img_labels + img_name + '.txt'

        # Move image
        img_to = imgs + type
        move(img_source, img_to)

        # Copy label
        label_to = img_labels + type           
        move(label_source, label_to)
               

# train Model
def train():
    options = 'python train.py --img 320 --batch 16 --epochs 30 --data facemask.yaml --cfg models/yolov5s.yaml --weights yolov5s.pt' 
    # --weights ./runs/train/exp2/weights/best.pt --standardize normalize_parameters.yml
    #subprocess.run(options, capture_output=True) # use to hide command line output
    subprocess.run(options) # use to show command line output 


#THIS PART COULD BE USED ON ANY DATASET TO MAKE PREDICTION 

def detection(model_dir, test_data_location, your_image_name):

    # make a prediction on test data
    weight = model_dir + '/weights/best.pt'

    test_data_location = test_data_location + your_image_name # use your own test data location

    options = f'python detect.py --source {test_data_location} --img-size 320 --conf 0.4 --weights {weight}' 
    subprocess.run(options, capture_output=True)

    #plot image with bounding boxes
    Image(model_dir + '/' + your_image_name)


if __name__ == "__main__" :
    
    # Data dir
    img_dir = './Dataset/own/images' + '/'      
    label_dir = './Dataset/own/labels' + '/'      
    
    # train&test split
    train_list, validate_list = train_test_split(img_dir)
    
    # Move data to train& test folder 
    move_data(train_list, label_dir, img_dir, "train")
    move_data(validate_list,  label_dir, img_dir, "validate") 
    
    
    #train and validate
    train()
    
    # make detection
    #test_data_location = 'Dataset/test/images/test/' # set path to your test data
    #your_image_name = ''
    #model_dir = os.listdir('/runs/train/')[-1] #get your training example
    #detection(model_dir, test_data_location, your_image_name)
   