import pandas as pd
import os
import yaml
from pprint import pprint
import random
import matplotlib.pyplot as plt
import albumentations as A
import cv2

# if category_flag=True always do 3 augmentations, 
# if category_flag=False, for images with class "mask_incorrectly" class we do 3 augmentations, and if there is no mask_incorrectly class we do 1 augmentation

def augmentation(path_to_images, path_to_labels, category_flag=False, save_to=None):
    
    if save_to != None:
        save_to_images = save_to + 'images/'
        save_to_labels = save_to + 'labels/'       
        if not os.path.exists(save_to_images):
            os.makedirs(save_to_images)
        if not os.path.exists(save_to_labels):
            os.makedirs(save_to_labels)           
    else:
        save_to_images = path_to_images
        save_to_labels = path_to_labels
        
    images = os.listdir(path_to_images)

    for image in images:
        
        image_name, ext = image.split(".")
        annotation = (path_to_labels + str(image_name) + ".txt")

        image = cv2.imread(path_to_images + image)
        width, height = image.shape[1], image.shape[0]
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        bboxes_df = pd.read_csv(annotation, delimiter = " ", header=None)
        category_ids = bboxes_df.iloc[:, 0].to_list()
        bboxes_df = bboxes_df.iloc[:,1:]    
        bboxes = bboxes_df.values.tolist()

        bboxess = []
        for bbox in bboxes:
            bbox = A.augmentations.bbox_utils.convert_bbox_to_albumentations (bbox=bbox, source_format="yolo", rows=height, cols=width)
            bbox = A.augmentations.bbox_utils.convert_bbox_from_albumentations (bbox=bbox, target_format="coco", rows=height, cols=width)
            bboxess.append(bbox)

        # comment in if you want augmentation only for the "mask_incorrectly class"
        if category_flag == True:
            augments = 3
        elif 1 in category_ids: # just mask_incorrectly will be augmented 3 times
            augments = 3
        else:
            augments = 1

        for i in range(augments):
            transformed = transform(image=image, bboxes=bboxess, category_ids=category_ids)
            cv2.imwrite(save_to_images + str(image_name) + "_augmentation_" + str(i) + ".jpg" ,transformed['image'])

            yolo_df = pd.DataFrame(columns=["class", "x_center", "y_center", "width", "height"])
            for j in range(len(transformed['bboxes'])):
                yolo_list = []
                cls = transformed['category_ids'][j]
                bbox_temp = transformed['bboxes'][j]
                bbox_temp = A.augmentations.bbox_utils.convert_bbox_to_albumentations(bbox=bbox_temp, source_format='coco',
                                                                            rows=height, cols=width)
                bbox_temp = A.augmentations.bbox_utils.convert_bbox_from_albumentations(bbox=bbox_temp,
                                                                                            target_format='yolo', rows=height,
                                                                                            cols=width)

                yolo_list.append(cls)
                yolo_list.extend(bbox_temp)

                # append list to dataframe which will be written to txt for considered image
                yolo_df.loc[len(yolo_df)] = yolo_list

                yolo_df['class'] = yolo_df['class'].astype(int)
                yolo_df.to_csv(save_to_labels + str(image_name) + "_augmentation_" + str(i) + ".txt", sep=" ", index=False, header=False)
        


def get_pipeline(apply=False, probability=0.0, mean=[0, 0, 0], std=[1, 1, 1]):
    pipeline = A.Compose(
        [
            A.HorizontalFlip(always_apply=False, p=0.5),
                A.OneOf([
                    A.RandomSizedBBoxSafeCrop(always_apply=False, p=0.5, height=239, width=168, erosion_rate=0.2, interpolation=1),
                    A.PiecewiseAffine(always_apply=False, p=0.5, scale=(0.03, 0.05), nb_rows=(4, 4), nb_cols=(4, 4), interpolation=1, mask_interpolation=0, cval=0, cval_mask=0, mode='constant', absolute_scale=False, keypoints_threshold=0.01),
                    
                    A.MotionBlur(always_apply=False, p=0.5, blur_limit=(5, 7)),
                    A.GaussNoise(always_apply=False, p=0.5, var_limit=[10.0, 20.0], per_channel=True, mean=0),
                ], p=1),
                A.OneOf([
                    A.RandomSizedBBoxSafeCrop(always_apply=False, p=0.5, height=190, width=261, erosion_rate=0.2, interpolation=1),
                    
                    A.Blur(always_apply=False, p=0.5, blur_limit=(6, 8)),
                    A.CLAHE(always_apply=False, p=0.5, clip_limit=(1, 4.0), tile_grid_size=(4, 4)),
                    A.HueSaturationValue(always_apply=False, p=0.5, hue_shift_limit=(-20, 20), sat_shift_limit=(-30, 30), val_shift_limit=(-20, 20)),
                ], p=1),
                A.OneOf([
                    A.RandomSizedBBoxSafeCrop(always_apply=False, p=0.5, height=266, width=216, erosion_rate=0.2, interpolation=1),
                    A.PiecewiseAffine(always_apply=False, p=0.5, scale=(0.03, 0.05), nb_rows=(4, 4), nb_cols=(4, 4), interpolation=1, mask_interpolation=0, cval=0, cval_mask=0, mode='constant', absolute_scale=False, keypoints_threshold=0.01),
                    
                    A.Downscale(always_apply=False, p=0.2, scale_min=0.25, scale_max=0.25, interpolation=0),
                    A.RGBShift(always_apply=False, p=0.5, r_shift_limit=(-20, 20), g_shift_limit=(-20, 20), b_shift_limit=(-20, 20)),
                    A.HueSaturationValue(always_apply=False, p=0.5, hue_shift_limit=(-20, 20), sat_shift_limit=(-30, 30), val_shift_limit=(-20, 20)),
                ], p=1),
                A.OneOf([
                    A.RandomSizedBBoxSafeCrop(always_apply=False, p=0.5, height=256, width=237, erosion_rate=0.2, interpolation=1),
                    A.ShiftScaleRotate(always_apply=False, p=0.5, shift_limit_x=(-0.0625, 0.0625), shift_limit_y=(-0.0625, 0.0625), scale_limit=(-0.09999999999999998, 0.10000000000000009), rotate_limit=(-45, 45), interpolation=1, border_mode=4, value=None, mask_value=None),
                    
                    A.RandomBrightness(always_apply=False, p=0.5, limit=(-0.2, 0.2)),
                    A.transforms.FancyPCA(always_apply=False, p=0.5, alpha=0.1),
                    A.GaussNoise(always_apply=False, p=0.5, var_limit=[10.0, 20.0], per_channel=True, mean=0),
                ], p=1),

        #A.transforms.Normalize(always_apply=apply, p=probability, mean=mean, std=std, max_pixel_value=255.0), 

        ], p=1.0, bbox_params={'format': 'coco', 'label_fields': ['category_ids'], 'min_area': 0.0, 'min_visibility': 0.0, 'check_each_transform': True}, keypoint_params=None, additional_targets={}
        )

            
    return pipeline


if __name__ == "__main__":
    
    path_to_normalize_parameters = ''
    path_to_images = './Dataset/own/images/train/'
    path_to_labels = './Dataset/own/labels/train/'
    #save_to = 'your/dir/' # will be created if not exist
    
    # augmentation pipeline
    transform = get_pipeline()
    
    random.seed(42)

    augmentation(path_to_images, path_to_labels, save_to=save_to)


