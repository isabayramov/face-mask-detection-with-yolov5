#!/bin/bash
#This script is used to launch inference by running the container and executing the inference of YOLOv5.


#These two docker commands allow to inject current best weights without running the container by copying them from the container image onto the host system with a dummy container
#This becomes relevant as soon as we do not clone the whole repository in deploy.sh anymore
#sudo docker create -ti --name dummy yolov5 bash
#sudo docker cp dummy:AISS-CV-Mask-Group-F/yolov5/runs/train/exp/weights/best.pt /home/aiss/Desktop/maskdetection/weights/best.pt

#needed if a inference session is started immediately after the preceding one was ended with control+c
sudo systemctl restart nvargus-daemon

#Needed to initialize stable connection of the nv-argus-daemon after a restart of the system. Can be omitted when running directly after building the docker image without restarting linux. Running it subsequently after closing a preceding session, this command will throw an error, but the model initation works anyways.
sudo timeout 6s gst-launch-1.0 nvarguscamerasrc ! 'video/x-raw(memory:NVMM),width=3264, height=2464, framerate=21/1, format=NV12' ! nvvidconv flip-method=0 ! 'video/x-raw,width=1640, height=1232' ! nvvidconv ! nvegltransform ! nveglglessink -e

sudo systemctl restart nvargus-daemon
sudo systemctl restart docker
#Command to run detect.py with specified location for weights of the model
#Following arguments are mainly derived from https://ngc.nvidia.com/catalog/containers/nvidia:deepstream-l4t and https://github.com/otamajakusi/dockerfile-yolov5-jetson/blob/main/run.sh
#xhost +local: allows every user to access the running x server, that is used for creating a GUI
#with -it we set docker in interactive mode to run it in an interactive shell
#with --rm we specify to remove container files after closing or exiting the container
#we again specify to use the nvidia container runtime with --runtime nvidia
#with --network host the container shares the same namespace as the host network,  becomes relevant if e.g. cameras are accessed over network
#with --device we mount devices that the container has access to, in our case our csi camera 
#with -e we set the environment variable DISPLAY and afterwards mount the X11-unix socket with -v to allow the container to render the correct display (adapted from https://ngc.nvidia.com/catalog/containers/nvidia:deepstream-l4t and https://github.com/otamajakusi/dockerfile-yolov5-jetson/blob/main/run.sh)
#we also mount our folder where we copied our best.pt weights file that we want the model to use during inference, and the argus socket to allow access to the csi camera (https://ngc.nvidia.com/catalog/containers/nvidia:deepstream-l4t)
#we then specify the container tag that should be run and the detect.py we want to execute from within the container
#all following arguments are parsed by detect.py and are configuration settings for our inference model. We specify the video soruce, where our weights are located and the confidence threshold for detection.
xhost +local:
sudo docker run -it --rm --runtime nvidia --network host --device /dev/video0:/dev/video0:mrw -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix -v /tmp/argus_socket:/tmp/argus_socket -v ~/Desktop/maskdetection/weights:/weights yolov5 python3 detect.py --source 0 --conf-thres 0.45 --img-size 320 --weights /weights/best.pt


